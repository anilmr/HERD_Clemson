from fabric.api import run, env, put
from fabric.tasks import execute

env.hosts = ["anilmr@node1.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node2.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node3.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node4.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node5.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node6.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node7.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node8.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node9.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node10.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node11.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node12.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
             "anilmr@node13.herd.cs6480phantomnet-PG0.clemson.cloudlab.us",
            ]

last_octet = 1

def rename():
    run("mv project_back project")

def kill_exp():
    run("cd /users/anilmr/project/HERD && bash local-kill.sh")

def install_vim():
    run("sudo apt-get -y install vim")

def assign_ip():
    global last_octet
    run("sudo ifconfig ib0 10.10.10." + str(last_octet) + "/24 up")
    last_octet += 1

def restart():
    run("sudo init 6")

def shm_init():
    run("sudo sysctl -w kernel.shmmax=2147483648 && sudo sysctl -w kernel.shmall=2147483648 && sudo sysctl -p /etc/sysctl.conf")

def install_hugepages():
    run('''sudo su -c "echo 4096 > /sys/devices/system/node/node0/hugepages/hugepages-2048kB/nr_hugepages"''')

def host_info():
    run('cat /etc/issue && uname -a')

def update_apt():
    run("sudo apt-get -y update")

def install_rdma():
    run('sudo apt-get -y install libibverbs1 ibverbs-utils librdmacm1 rdmacm-utils libmlx4-1 librdmacm-dev libibverbs-dev infiniband-diags ibutils ibverbs-utils qlvnictools srptools rds-tools rdmacm-utils perftest libmthca1 libmlx4-1 libipathverbs1')

def install_java():
    run("sudo apt-get -y install default-jdk")

def copy_file():
    put('load_ib.sh','/users/anilmr')

def check_rdmacm_modules():
    run('modinfo rdma_cm | head -1')

def load_modules():
    run('sudo sh load_ib.sh')

def check_modules():
    run('lsmod | grep rdma_ucm')

def check_ibvdevices():
    run('ibv_devices')

def install_git():
    run('sudo apt-get install git')

def copy_project():
    put('/Users/anil/Research/project_clemson','/users/anilmr')
    run('mv project_clemson project')

def install_project():
    run('cd project/HERD && make clean && make')

def copy_server_file():
    put('servers','/users/anilmr/project/HERD')

def copy_header_file():
    put('common.h','/users/anilmr/project/HERD')

def configure_hugepages():
    run('sudo bash /users/anilmr/project/HERD/scripts/hugepages-create.sh 0 4096')
    run('cat /proc/meminfo | grep Huge')

def rping():
    run("rping -c -a 10.10.10.1 -C1")

def configure_zipf():
    run("cd project/HERD/YCSB/src/ && sudo rm -rf /dev/zipf && sudo mkdir /dev/zipf && sudo java zipf")

