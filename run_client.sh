#!/bin/bash

# Just put all the client nodes into the below list.

username=anilmr
id=0
for i in `seq 2 13`;
do
  ssh ${username}@node${i}.herd.cs6480phantomnet-PG0.clemson.cloudlab.us "cd project/HERD && sudo sh local-kill.sh" 
	ssh -oStrictHostKeyChecking=no ${username}@node${i}.herd.cs6480phantomnet-PG0.clemson.cloudlab.us "cd project/HERD && sudo rm -rf client-tput && mkdir client-tput"
	ssh -oStrictHostKeyChecking=no ${username}@node${i}.herd.cs6480phantomnet-PG0.clemson.cloudlab.us "cd project/HERD && sudo sh run-machine.sh $id" &
	sleep 1
	id=`expr $id + 1`
	echo $id
done
