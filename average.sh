rm temp
DIR=`pwd`
total_sum=0
total_files=3
for file in $DIR/*; do
	echo "Processing file $file"
	grep "IOPS" $file >> temp
	total_files=`expr $total_files + 1`
done

sum=`awk '{ sum += $3 } END { print sum }' temp`
echo "Sum = $sum"
N=`wc -l temp | awk '{print $1;}'`
echo $N
avg=`python -c "print $sum / $N"`
echo $avg

throughput=`python -c "print $avg * 36"`
echo "Directory DIR's average = $avg, throughput $throughput" 

