#!/bin/bash

# Just put all the client nodes into the below list.

client_nodes=(apt096 apt124 apt094 apt087 apt100 apt107 apt110 apt077 apt116 apt122 apt076 apt118 apt074 apt089 apt104 apt082 apt083)
username=anilmr

rm -rf client
mkdir client && cp average.sh client/
for i in `seq 2 14`;
do
	ssh -oStrictHostKeyChecking=no ${username}@node${i}.herd.cs6480phantomnet-PG0.clemson.cloudlab.us hostname
	scp -oStrictHostKeyChecking=no ${username}@node${i}.herd.cs6480phantomnet-PG0.clemson.cloudlab.us:/users/${username}/project/HERD/client-tput/* client/ 
done
